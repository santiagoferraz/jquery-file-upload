<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

error_reporting(E_ALL | E_STRICT);
//error_reporting(E_STRICT);
require('UploadHandler.php');
$upload_handler = new UploadHandler();

//echo ini_get('upload_max_filesize').'<br/>';
//ini_set("upload_max_filesize","300M");
//echo ini_get("upload_max_filesize");