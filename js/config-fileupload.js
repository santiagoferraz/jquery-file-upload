$(function() {
   
    'use strict';
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        //maxFileSize: 100000000, //10 MB
        // Setup Doka Image Editor (https://gumroad.com/a/455455859):
        doka: Doka.create(),
        edit:
        Doka.supported() &&
        function(file) {
            return this.doka.edit(file).then(function(output) {
            return output && output.file;
            });
        },
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: 'server/php/'
        });

        // Enable iframe cross-domain access via redirect option:
        $('#fileupload').fileupload(
            'option',
            'redirect',
            window.location.href.replace(/\/[^/]*$/, '/cors/result.html?%s'),
    );
    
});

// Desabilitar tecla ENTER para que o form não seja submetido ao teclar
$(document).keypress(function(event){
    if (event.which == '13') {
        event.preventDefault();
    }
});

$('#botCarrega').on('click', function (e) {

    // RECUPERA O ARQUIVOS
    var data = $('.template-upload').data('data');

    if(data) {
        // RENOMEIA OS ARQUIVOS
        renameFiles(data);

        //VERIFICA SE O TAMANHO DA ALTURA É METADE DA LARGURA
        validaWidhtHeight(data).then(() => {
            if(data.originalFiles.length > 1) {
                // VERIFICA SE TODAS AS IMAGENS SÃO DO MESMO TAMANHO
                isSizeEquals(data).then(() => {
                    // SUBMIT
                    $('.start').click();
                }).catch(() => {
                    let msgErro = 'Imagens de tamanhos diferentes! Todas as imagens devem ter o mesmo tamanho';
                    openModalJquery(msgErro);
                    return;
                });
            } else {
                // SUBMIT
                $('.start').click();
            }

        }).catch(() => {
            let msgErro = 'A altura da imagem não equivale a metade da largura! As imagens devem ter a altura igual a metade da largura.';
            openModalJquery(msgErro);
            return;
        });
    }

});

// RENOMEIA OS ARQUIVOS ANTES DO ENVIO
function renameFiles(data) {
    $.each(data.originalFiles, function (index, file) {
        let inputs = data.context.find(':input');
        let newname = $('.nome-arquivo')[index].value;
        Object.defineProperty(data.originalFiles[index], 'name', {
            value: newname
        });
    });
}

// ABRE MODAL DE AVISO
function openModalJquery(mensagem) {
    $('#modal-jquery p').text(mensagem);
    $('#modal-jquery').modal({
        escapeClose: false,
        clickClose: false,
        showClose: false
    });
}

// VERIFICA SE A ALTURA É METADE DA LARGURA
function validaWidhtHeight(data) {
    return new Promise((resolve, reject) => {
        let image;
        for (let index = 0; index < data.originalFiles.length; index++) {
            if (data.originalFiles[index]) {
                image = new Image();
                image.src = window.URL.createObjectURL(data.originalFiles[index]);
                image.onload = function() {
                    if(!(this.height === (this.width / 2))) {
                        reject();
                    }
                    if(index === (data.originalFiles.length-1)) {
                        resolve();
                    }
                };
            }
        }
    });
}

// VERIFICA SE TODAS AS IMAGENS TEM O MESMO TAMANHO
function isSizeEquals(data) {
    return new Promise((resolve, reject) => {
        let image;
        let sizes = [];
        for (let index = 0; index < data.originalFiles.length; index++) {
            if (data.originalFiles[index]) {
                image = new Image();
                image.src = window.URL.createObjectURL(data.originalFiles[index]);
                image.onload = function() {
                    sizes.push({height: this.height, width: this.width});
                    if(sizes.length > 1) {
                        if((sizes[0].height !== sizes[sizes.length - 1].height) ||
                            sizes[0].width !== sizes[sizes.length - 1].width) {
                            reject();
                        }
                    }
                    if(index === (data.originalFiles.length-1)) {
                        resolve();
                    }
                };
            }
        }
    });
}

$('#fileupload').fileupload({
    // ... some options ...
    maxFileSize: 20000000, //20 MB
    maxChunkSize: 1000000, // 1 MB
    previewThumbnail:       true,
    loadImageMaxFileSize:   20000000000, // 1000MB Liberou thumbs com mais de 10mb
    messages : {
      maxNumberOfFiles: 'AAA Maximum number of files exceeded',
      acceptFileTypes: 'AAA File type not allowed',
      maxFileSize: 'Este arquivo é muito grande, baixe para menos de 10MB',
      minFileSize: 'AAA File is too small',
      uploadedBytes : 'AAA Uploaded bytes exceed file size'
    },
    
    function (e, data) {
        console.log('Processing ' + data.files[data.index].name + ' failed.');
    }
    // ... some other options ...
  })
  .on("fileuploadprocessfail", function(e, data) {
    var file = data.files[data.index];
    //alert(file.error);    
});

// Controla a area do Drop and drag
$(document).bind('dragover', function (e) {
    var dropZone = $('#zdrop'),
        timeout = window.dropZoneTimeout;
    if (timeout) {
        clearTimeout(timeout);
    } else {
        dropZone.addClass('in');
    }
    var hoveredDropZone = $(e.target).closest(dropZone);
    dropZone.toggleClass('hover', hoveredDropZone.length);
    window.dropZoneTimeout = setTimeout(function () {
        window.dropZoneTimeout = null;
        dropZone.removeClass('in hover');
    }, 100);
    $('#esconderMensagem1').hide();
    $('#botCarrega').show('slow'); 
    $('#esconderMensagem2').hide();        
    $('#botCarrega2').show('slow'); 
    $("#botAdd").removeClass("btn-success");              
    $("#botAdd").addClass("btn-warning");        
    $("#botAdd").removeClass("btn-lg");
    $('#zdrop').hide(500);  
}) 

// Controla o botão "Adicione arquivos"
$('#botAdd').click(function(){
    $('#esconderMensagem1').hide();
    $('#botCarrega').show('slow'); 
    $('#esconderMensagem2').hide();        
    $('#botCarrega2').show('slow'); 
    $("#botAdd").removeClass("btn-success");              
    $("#botAdd").toggleClass("btn-warning");        
    $("#botAdd").removeClass("btn-lg");
    $('#zdrop').hide();              
});
// Controla o botão "Voltar"
$('#voltarEscolha').click(function(){
    //$('#fileupload').fileupload('destroy');
    $('#uploadProduto').hide();
    $('#pagEscolha').show('slow'); 
    $('#voltarEscolha').hide();
    history.back();     
});  

//Retorna o numero de arquivos
var number_of_files = 0; // or sessionStorage.getItem('number_of_files')      
$('#fileupload').bind('fileuploadadd', function (e, data) {
  $.each(data.files, function (index, file) {
    //console.log('Added file in queue: ' + file.name);
    number_of_files = number_of_files + 1;
    //sessionStorage.setItem('number_of_files', number_of_files);    


    /*Cria imagens em outras medidas
    $.blueimp.fileupload.prototype.processActions.duplicateImage = function (data, options) {
        if (data.canvas) {
            data.files.push(data.files[data.index]);
            console.log(data);
        }
        return data;
        
    };
    
    $('#fileupload').fileupload({
        processQueue: [
            {
                action: 'loadImage',
                fileTypes: /^image\/(gif|jpeg|png)$/,
                maxFileSize: 20000000 // 20MB
            },
            {
                action: 'resizeImage',
                maxWidth: 1920,
                maxHeight: 1200
            },
            {action: 'saveImage'},
            {action: 'duplicateImage'},
            {
                action: 'resizeImage',
                maxWidth: 1280,
                maxHeight: 1024
            },
            {action: 'saveImage'},
            {action: 'duplicateImage'},
            {
                action: 'resizeImage',
                maxWidth: 1024,
                maxHeight: 768
            },
            {action: 'saveImage'}
        ]
    });    */
    
    

    
  });
}).bind('fileuploadprocessdone', function (e, data) {
    console.log(number_of_files + " arquivos incluidos");

    // ADICIONA O ARQUIVO NA LISTA DE FILES
    setFileInList(data)
});

// ADICIONA OS ARQUIVOS NA LISTA DE FILES (O PLUGIN NÃO ESTAVA ADICIONANDO QUANDO ERA EM DUAS ETAPAS)
function setFileInList(data) {
    let files = $('.template-upload').data('data').originalFiles;
    let isFileExists = false;
    $.each(files, function (index, f) {
        if(f.name === data.files[0].name) {
            isFileExists = true;
        }
    });
    if(!isFileExists) {
        $('.template-upload').data('data').originalFiles[files.length] = data.files[0];
    }
}

/*      
//Retorna o largura e altura da imagem -TOUR VIRTUAL
$('#fileupload').bind('fileuploadadd', function (e, data) {
var reader = new FileReader();
data.files.forEach(function (item, index) {
    reader.readAsDataURL(item);
    reader.data = data;
    reader.file = item;
    reader.onload = function (_file) {
        var image = new Image();
        image.src = _file.target.result;
        image.file = this.file;
        image.data = this.data;
        image.onload = function () {
            var w = this.width,
                h = this.height,
                n = this.file.name;
            
            if (h === w/2) {
                //console.log("ta ok");
            }else {
               data.files.error = true;
                item.error = "Imagem fora do padrão equiretangular";
                var error = item.error;
                $(data.context[index]).find(".error").text(error); 
            }
            console.log("A largura é de "+ w +" e a altura é "+ h);
        };
    };
});
})
*/